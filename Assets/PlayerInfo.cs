﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInfo {
    public float speed = 0.25F;
    public bool grounded = false;
    public bool idle = false;
    public float facingRight = 1.0f;

    public int maxUpSpeed = 5;
    public int maxLeftSpeed = -5;
    public int maxRightSpeed = 5;

    public Transform transform;
    public Rigidbody2D rigidBody2D;
    public GameObject gameObject;
    public Animator animator;
    public PlayerInfo(PlayerScript playerScript, Animator animator)
    {
        this.transform = playerScript.transform;
        this.rigidBody2D = playerScript.GetComponent<Rigidbody2D>();
        this.gameObject = playerScript.gameObject;
        this.animator = animator;
    }
}
