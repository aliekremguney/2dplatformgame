﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerUtil {

    PlayerInfo playerInfo;
    public PlayerUtil(PlayerInfo playerInfo)
    {
        this.playerInfo = playerInfo;
    }

    public bool CheckPlayerLeftScreen()
    {
        if (playerInfo.transform.position.y < -10)
        {
            Debug.Log("PlayerUtil --> player has left the scene.");
            return true;
        }
        else
        {
            return false;
        }
    }

    public void SpawnPlayerToCenter()
    {
        //Respawn in center
        playerInfo.transform.position = new Vector3(0, 5, 0);
    }

    public void SetAnimationForPlayer()
    {
        //hierarchy of animations
        if (!playerInfo.grounded)
        {
            playerInfo.animator.Play("JumpAnimation");
        }
        else if (playerInfo.grounded && playerInfo.rigidBody2D.velocity.x == 0)
        {
            playerInfo.animator.Play("IdleAnimation");
        }
        else if (playerInfo.grounded && playerInfo.rigidBody2D.velocity.x != 0)
        {
            playerInfo.animator.Play("WalkAnimation");
        }
    }

    public void MovePlayerUp()
    {
        //update player info
        playerInfo.idle = false;
        playerInfo.grounded = false;
      
        //update player physics
        if (playerInfo.rigidBody2D.velocity.y < playerInfo.maxUpSpeed)
        {
            playerInfo.rigidBody2D.velocity += new Vector2(0, (7 - playerInfo.rigidBody2D.velocity.y));
        }
    }

    public void MovePlayerLeft()
    {
        //update player info
        if (playerInfo.facingRight == 1.0f)
        {
            playerInfo.facingRight = -1.0f;
            FlipPlayer();
        }
        playerInfo.idle = false;

        //update player physics
        if (playerInfo.rigidBody2D.velocity.x > playerInfo.maxLeftSpeed)
        {
            playerInfo.rigidBody2D.velocity += new Vector2(-1, 0);
        }
    }

    public void MovePlayerRight()
    {
        //update player info
        if (playerInfo.facingRight == -1.0f)
        {
            playerInfo.facingRight = 1.0f;
            FlipPlayer();
        }
        playerInfo.idle = false;

        //update player physics
        if (playerInfo.rigidBody2D.velocity.x < playerInfo.maxRightSpeed)
        {
            playerInfo.rigidBody2D.velocity += new Vector2(1, 0);
        }
    }

    public void FlipPlayer()
    {
        Vector2 theScale = playerInfo.transform.localScale;
        theScale.x *= -1;
        playerInfo.transform.localScale = theScale;
    }

    public void CollidePlayerWithWallObject(Collision2D coll)
    {
        if (playerInfo.transform.position.y >= coll.gameObject.transform.position.y)
        {
            //Debug.Log(playerInfo.gameObject.name + "-- collided with --" + coll.gameObject.name);
            playerInfo.grounded = true;
            playerInfo.idle = true;
            playerInfo.animator.Play("IdleAnimation");
        }
    }
    
}
