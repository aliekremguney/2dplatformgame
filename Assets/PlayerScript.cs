﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerScript : MonoBehaviour {

    PlayerInfo playerInfo;
    PlayerUtil playerUtil;

    void Start ()
    {
        //Player Info Model
        playerInfo = new PlayerInfo(this, GetComponent<Animator>());
        //Player Util
        playerUtil = new PlayerUtil(playerInfo);
    }

    //For player physics detections
    private void FixedUpdate()
    {
        //debug purposes
        if (playerUtil.CheckPlayerLeftScreen())
        {
            playerUtil.SpawnPlayerToCenter();
        }
        
        //player input handle
        if (Input.GetKey(KeyCode.W) && playerInfo.grounded)
        {
            playerUtil.MovePlayerUp();
        }

        if (Input.GetKey(KeyCode.A))
        {
            playerUtil.MovePlayerLeft();
        }
        if (Input.GetKey(KeyCode.D))
        {
            playerUtil.MovePlayerRight();
        }

        //check and set player animation
        playerUtil.SetAnimationForPlayer();
    }

    void OnCollisionEnter2D(Collision2D coll)
    {
        if (coll.gameObject.tag == "Wall")
        {
            playerUtil.CollidePlayerWithWallObject(coll);
        }
    }
}
